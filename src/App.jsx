import React, { useRef } from 'react'
import Forminput from './Components/Forminput';
import { useState } from 'react';

const App = () => {

  const [values, setValues] = useState({
    username: '',
    email: '',
    birthday: '',
    password: '',
    confirmPassword: '',
  });
 
  const inputs = [
    {
      id:1,
      name:"username",
      type:"text",
      placeholder:"Username",
      label:"Username",
      errorMessage: "Username should be 3-16 character and sholdn't include any special charactor!",
      required: true,
      pattern: "^[A-Za-z0-9]{3,16}$"
    },
    {
      id:2,
      name:"email",
      type:"email",
      placeholder:"Email",
      label:"Email",
      errorMessage: "It should be a valid email address!",
      required: true,
    },
    {
      id:3,
      name:"birthday",
      type:"date",
      placeholder:"birthday",
      label:"birthday",
    },
    {
      id:4,
      name:"password",
      type:"password",
      placeholder:"Password",
      label:"Password",
      errorMessage: "Password should be 8-20 charactor and include at least 1 letter, 1 number and special 1 charactor! ",
      required: true,
      pattern: `^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$`
    },
    {
      id:5,
      name:"confirmPassword",
      type:"password",
      placeholder:"Confirm Password",
      label:"Confirm Password",
      errorMessage: "Password don't match!",
      required: true,
      pattern: values.password,
    }
  ]
  

  const handleSubmit = (e) =>{
    e.preventDefault();
  }
  const onChange = (e) =>{
    setValues({...values, [e.target.name]: e.target.value})
  }

  console.log(values);
   
  return (
    <>
       <div className="app">
         <form onSubmit={handleSubmit}>
         <h1>Register</h1>
         {inputs.map((input)=>(

           <Forminput 

           key={input.id} 
           {...input} 
           value={values[input.name]}
           onChange={onChange} 

           />
           ))}
           <button>Submit</button>
         </form>
       </div>
    </>
  )
}

export default App;
