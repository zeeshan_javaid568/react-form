import React, { useState } from 'react';
import './formInput.css';

const Forminput = (props) => {

    const {label, onChange, errorMessage, id, ...inputprops} = props;
    const [focused, setFocused] = useState(false);

    const handleFocus = (e) =>{
        setFocused(true);
    }
    
    return (
        <>
            <div className="forminput">
            <label>{label}</label>
            <input 
            {...inputprops} 
            onChange={onChange} 
            onBlur={handleFocus} 
            focused={focused.toString()}
            onFocus={()=>inputprops.name==="confirmPassword" && setFocused(true)}    
            />
            <span>{errorMessage}</span>
            </div>
        </>
    )
}

export default Forminput;
